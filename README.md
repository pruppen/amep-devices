# Device controllers for automated microfluidic electroporation

Python library with high level functions for USB control of the devices needed for automated microfluidic electroporation.
