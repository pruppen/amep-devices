import serial
from time import time, sleep
import logging

import matplotlib.pyplot as plt
from threading import Thread
import numpy as np


class BioshakeDevice:

    _DEFAULT_SPEED_TARGET = 500

    def __init__(self, port='COM3', timeout=1):
        self.t_start = time()
        self.read_value = np.zeros((1, 2))
        self.read_values = np.empty((0, 2))

        self.data = {'time': [],
                     'temperature': {'time': 0, 'time_record': [], 'current': 0, 'record': [], 'target': 0,
                                     'target_record': []},
                     'shaking': {'time': 0, 'time_record': [], 'current': 0, 'record': [], 'target': 0,
                                 'target_record': []}
                     }

        self.busy = False

        self._serial_device = serial.Serial(port, timeout=timeout)

        self._serial_device.write(b'shakeGoHome\r')
        sleep(3)

        print(self._serial_device.read(self._serial_device.in_waiting))

    def _args_to_request(self, args):
        request = args + '\r'
        request = bytes(request, 'UTF-8')
        return request

    def _send_request(self, request):

        """
        Sends request to bioshake device over serial port and
        returns number of bytes written
        """

        # request = self._args_to_request(*args)
        # self._debug_print('request', request)
        bytes_written = self._serial_device.write(request, delay_write=True)
        return bytes_written

    def _send_request_get_response(self, request):
        """
        Sends request to device over serial port and
        returns response
        """
        request = self._args_to_request(request)
        self._serial_device.write(request)
        sleep(2)
        response = self._serial_device.read(self._serial_device.in_waiting)
        # response = self._serial_device.write(request, use_readline=True, check_write_freq=True)
        response = response.strip()
        if response == 'e':
            request = request.strip()
        return response

    def temp_on(self, temp_target):
        """
        Activate the temperature control. temp_target allowed range: 0 – 99.0 (°C)
        """
        response = self._set_temp_target(temp_target)
        if response is not None:
            return self._send_request_get_response('tempOn')

    def temp_off(self):
        """
        Deactivate the temperature control.
        """
        return self._send_request_get_response('tempOff')

    def get_temp_target(self):
        """
        Return the target temperature. (°C)
        """
        self.read_value = float(self._send_request_get_response('getTempTarget'))
        return self.read_value

    def _set_temp_target(self, temp_target):
        """
        Set the target temperature in °C allowed range: 0 – 99.0 in °C
        """
        if (temp_target >= 0) and (temp_target <= 99):
            return self._send_request_get_response('setTempTarget' + str(int(round(temp_target * 10))))
        else:
            print(self._set_temp_target.__doc__)

    def get_temp_actual(self):
        """
        Return the actual temperature. (°C)
        """
        self.read_value = np.zeros((1, 2))
        self.read_value[0, 0] = time() - self.t_start
        self.data['temperature']['time'] = time() - self.t_start

        # self.read_value[0, 1] = float(self._send_request_get_response('getTempActual'))
        self.data['temperature']['current'] = float(self._send_request_get_response('getTempActual'))
        self.read_value[0, 1] = self.data['temperature']['current']
        logging.debug("t: {} s, T = {}°C".format(self.data['temperature']['time'], self.data['temperature']['current']))
        return self.read_value

    def measurement_thread(self):
        self.run_condition = True
        logging.info("bioshake measurements started (thread)")
        self.initial_time = time()
        while self.run_condition:
            try:
                self.get_temp_actual()
                print("thread: {}".format(self.read_value))
                self.read_values = np.vstack((self.read_values, self.read_value))
            except:
                logging.info("ValueError: could not convert string to float")

        logging.info("Measurement thread: stopped")
        # print("Measurement thread: stopped")

    def start_measurement_thread(self):
        y = Thread(target=self.measurement_thread)
        y.start()

    def stop_measurement_thread(self):
        self.run_condition = False

    def measurement_timer(self, duration):
        self.timer_t_start = time()
        while time() - self.timer_t_start < duration:
            self.get_temp_actual()
            print("timer: {}".format(self.read_value))
            self.read_values = np.vstack((self.read_values, self.read_value))

    #     def safe_data_to_file(self, filename='arduino_data.csv', header='', delimiter=','):
    #         # save pressure and flow data to file
    #         np.savetxt(filename, self.read_values, header=header, delimiter=delimiter)

    def heat_until(self, temp_target, treshold):
        if treshold > temp_target:
            treshold = temp_target
            logging.INFO("heat_until: treshold above target temperature. Treshold set to target temperature.")

        self.temp_on(temp_target)

        self.get_temp_actual()
        # print("heat_until_0: {}".format(self.read_value))
        self.read_values = np.vstack((self.read_values, self.read_value))
        while self.read_value[0, 1] < treshold:
            self.get_temp_actual()
            print("heat_until: {}".format(self.read_value))
            self.read_values = np.vstack((self.read_values, self.read_value))

    def cool_until(self, temp_target, treshold):
        if treshold < temp_target:
            treshold = temp_target
            logging.INFO("cool_until: treshold below target temperature. Treshold set to target temperature.")

        self.temp_on(temp_target)

        self.get_temp_actual()
        # print("cool_until_0: {}".format(self.read_value))
        self.read_values = np.vstack((self.read_values, self.read_value))
        while self.read_value[0, 1] > treshold:
            self.get_temp_actual()
            print("cool_until: {}".format(self.read_value))
            self.read_values = np.vstack((self.read_values, self.read_value))

    def _set_shake_speed_target(self, speed_target):
        '''
        Set the target mixing speed. Allowable range: 200 – 3000 rpm
        '''
        if (speed_target >= 200) and (speed_target <= 3000):
            return self._send_request_get_response('setShakeTargetSpeed' + str(int(speed_target)))
        else:
            print(self._set_shake_speed_target.__doc__)

    def shake_on(self, speed_target=_DEFAULT_SPEED_TARGET):
        '''
        Start the shaking at the target speed (rpm) or with the default
        speed if no speed_target provided.
        '''
        response = self._set_shake_speed_target(speed_target)
        if response is not None:
            return self._send_request_get_response('shakeOn')

    def shake_on_with_runtime(self, runtime, speed_target=_DEFAULT_SPEED_TARGET):
        '''
        Shake for runtime duration (s) at the speed_target (rpm) or with
        the default speed_target if none provided. Allowable runtime
        range: 0 – 99999 seconds.
        '''
        response = self._set_shake_speed_target(speed_target)
        if response is not None:
            return self._send_request_get_response('shakeOnWithRuntime'+str(int(runtime)))

    def get_shake_remaining_time(self):
        '''
        Return the remaining time in seconds.
        '''
        return int(float(self._send_request_get_response('getShakeRemainingTime')))

    def shake_off(self):
        '''
        Stop the shaking and return to the home position.
        '''
        return self._send_request_get_response('shakeOff')

    #
    def plot_data(self, save_to_fig=False, save_file_name="bioshake_plot.png", legend="", title="", show=True):
        # Plot results
        fig, axes = plt.subplots(1, 1)
        timepoints = self.read_values[:, 0]  # time as x-axis

        if legend == "":
            legend = ["Temperature in bioshake [°C]"]

        # plot serial reads vs time
        axes.plot(timepoints, self.read_values[:, 1])

        if title == "":
            axes.set_title('bioshake temperature control')

        axes.set(xlabel='time [s]', ylabel='Temperature in bioshake [°C]')
        axes.legend(legend)

        # save image of plot is True
        if save_to_fig is True:
            plt.savefig(save_file_name)

        if show is True:
            plt.show()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    q1 = BioshakeDevice()
    # print(q1._send_request_get_response("b'getTempMax\r'"))
    T_hot = 27
    T_cool = 22
    q1.shake_on()
    # q1.start_measurement_thread()
    # sleep(20)
    # q1.set_temp_target(20)
    # q1.temp_on(42)
    q1.heat_until(T_hot, T_hot - 1)
    q1.measurement_timer(2)
    # q1.temp_on(4)
    q1.cool_until(T_cool, T_cool + 2)
    q1.measurement_timer(2)
    # sleep(20)
    # q1.stop_measurement_thread()
    q1.temp_off()
    q1.shake_off()

    q1.plot_data()

    # print("read_values: {}".format(q1.read_values))
