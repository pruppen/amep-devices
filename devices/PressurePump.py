from Fluigent.SDK import fgt_detect, fgt_init, fgt_close, fgt_get_sensorValue, fgt_set_sensorRegulation, fgt_set_pressure, fgt_get_pressure
import logging
from time import time, sleep
import numpy as np
import sys
import matplotlib.pyplot as plt
from threading import Thread


class PressurePump:

    def __init__(self, number_of_pumps=2, name="FlowEZ"):
        self.name = name
        self.number_of_pumps = number_of_pumps
        self.error = 0
        self.connected = 0

        self.initialize()
        self.Sns = 0

        self.initial_time = time()
        self.sensor_read = np.zeros((1, 1+2*number_of_pumps))
        self.sensor_data = np.empty((0, 1+2*number_of_pumps))

    def initialize(self):
        # initialize Fluigent pressure pump
        fgt_detect()
        text = 'initialize pressure pump'
        logging.info(text)

        self.SNs, types = fgt_detect()
        if len(self.SNs) > 0:
            fgt_init()
            text = "Connection with Fluigent pump OK! SN: {}".format(self.SNs)
            logging.info(text)
        else:
            text = "no connection with Fluigent pump possible!"
            logging.info(text)

    def check(self):
        # set pressure of both pumps
        for i in range(self.number_of_pumps):
            fgt_set_pressure(i, 100)
        sleep(10)
        # set flowrate of both pumps
        for i in range(self.number_of_pumps):
            fgt_set_sensorRegulation(i, i, 10)
        input("Check for stable pressure and flowrate in Fluigent AiO software. Check if liquid leaves the channel and that there are no leaks.")
        # write pressure to file
        pressure = fgt_get_pressure(0, include_timestamp=False)
        sleep(1)
        for i in range(self.number_of_pumps):
            fgt_set_pressure(i, 0)

    def set_pressure(self, channel=0, pressure=0):
        fgt_set_pressure(channel, pressure)
        text = "Pressure of channel {} set to {} mbar!".format(channel, pressure)
        logging.info(text)

    def set_pressures(self, pressures):
        if len(pressures) == self.number_of_pumps:
            for i, pressure in enumerate(pressures):
                self.set_pressure(i, pressure)

    def set_flowrate(self, pump_channel=0, sensor_channel=None, flow_rate=0):
        if sensor_channel is None:
            sensor_channel = pump_channel
        fgt_set_sensorRegulation(pump_channel, sensor_channel, flow_rate)
        text = "Flow rate of channel {} set to {} uL/min (sensor {}) !".format(pump_channel, flow_rate, sensor_channel)
        logging.info(text)

    def set_flows(self, flow_rates):
        if len(flow_rates) == self.number_of_pumps:
            for i, flow in enumerate(flow_rates):
                self.set_flowrate(i, i, flow)
        else:
            text = 'set_flows: input incorrect'
            logging.info(text)

    def get_pressure(self, pump_channel):
        pressure = fgt_get_pressure(pump_channel)
        return pressure

    def get_flow(self, sensor_channel):
        flow_rate = fgt_get_sensorValue(sensor_channel)
        return flow_rate

    def read_sensors(self):
        # Read sensor values and saves them as list in self.sensor_read ([time, Q1, Q2, P1, P2])
        self.sensor_read = np.zeros((1, 1 + 2*self.number_of_pumps))
        self.sensor_read[0, 0] = time()-self.initial_time
        # read flow rates
        for i in range(self.number_of_pumps):
            self.sensor_read[0, i+1] = self.get_flow(i)
            self.sensor_read[0, i+1+self.number_of_pumps] = self.get_pressure(i)

    def measurement_timer(self, duration, message='start timer', time_between_measurements=1):
        logging.info(message)
        start_time = time()
        while time() - start_time < duration:
            self.read_sensors()
            self.sensor_data = np.vstack((self.sensor_data, self.sensor_read))
            sys.stdout.write("\r")
            sys.stdout.write("{:2d} seconds remaining.".format(duration - (int(time() - start_time))))
            sys.stdout.flush()
            sleep(time_between_measurements)  # sample every second
        print("")

    def measurement_thread(self, time_between_measurements=0.1):
        self.run_condition = True
        print(time_between_measurements)
        start_time = time.time()
        logging.info("Pressure pump measurements started (thread)")
        while self.run_condition:
            self.read_sensors()
            self.sensor_data = np.vstack((self.sensor_data, self.sensor_read))
            time.sleep(time_between_measurements)  # sample every second
        logging.info("Measurement thread: stopped")
        # print("Measurement thread: stopped")

    def start_measurement_thread(self, time_between_measurements=0.1):
        y = Thread(target=self.measurement_thread, args=(time_between_measurements,))
        y.start()

    def stop_measurement_thread(self):
        self.run_condition = False

    def save_data_to_file(self, filename='data.csv', header='', delimiter=','):
        # save pressure and flow data to file
        np.savetxt(filename, self.sensor_data, header=header, delimiter=delimiter)

    def plot_data(self, save_to_fig=False, save_file_name="plot.png", legend="", show=True):
        # Plot results
        fig, axes = plt.subplots(1, 2)
        time = self.sensor_data[:, 0]

        if legend == "":
                legend = ["Channel {}".format(n) for n in range(1, self.number_of_pumps+1)]

        # flow rate plot
        for i in range(1, self.number_of_pumps+1):
            axes[0].plot(time, self.sensor_data[:, i])

        axes[0].set_title('Flow rate')
        axes[0].set(xlabel='time [s]', ylabel='flow rate [uL/min]')
        axes[0].legend(legend)

        # plot pressure
        for i in range(self.number_of_pumps + 1, 2 * self.number_of_pumps+1):
            axes[1].plot(time, self.sensor_data[:, i])

        axes[1].set(xlabel='time [s]', ylabel='pressure [mbar]')
        axes[1].legend(legend)
        axes[1].set_title('Pressure')

        if save_to_fig is True:
            plt.savefig(save_file_name)

        if show is True:
            plt.show()

    def close(self):
        # set pressure to zero and close connection
        for i in range(self.number_of_pumps):
            self.set_pressure(i, 0)
        fgt_close()


if __name__ == "__main__":
    # configure logging to output to console
    logging.basicConfig(level=logging.INFO)

    # create object of the fluigent pressure pump
    pump = PressurePump(number_of_pumps=2)

    # sleep(1)
    pump.set_pressure(0, 20)
    pump.set_pressure(1, 30)

    pump.measurement_timer(duration=5, message='start timer', time_between_measurements=0.1)

    # pump.save_data_to_file()

    pump.close()

    # Plot results
    pump.plot_data()








