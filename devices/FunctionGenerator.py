from afg2225library.AFG2225 import AFG2225
import logging
from time import sleep


class FunctionGenerator:

    def __init__(self, name='AFG2225'):
        self.name = name
        self.error = 0
        self.connected = 0
        self.piezo_check_passed = 0

        self.initialize()

    def initialize(self):
        # initialize function generator
        self.instr = AFG2225()

        # check connection to instrument
        self.connected, _ = self.instr.check_connection()
        if self.connected == 0:
            # print(SN + ' connected.')
            print("...Connection with function generator (" + self.instr.port + ") OK!")
        else:
            print('No device connected')
            self.error = 1

    def check_piezo(self, channel=1, amplitude=3, frequency=12, frequency_unit='kHz'):
        # 12 kHz is a good frequency to test the piezo by hearing a high pitched sound
        self.instr.set_waveform('sine', channel=1)
        self.instr.set_amplitude(amplitude, channel=1)
        self.instr.set_frequency(frequency, frequency_unit, channel=channel)
        self.instr.turn_on()
        check = input("Checking piezo at {} {}. Press enter if you can hear a sound. Enter comment to skip check."
                      .format(frequency, frequency_unit))
        if check != '':
            logging.info("Warning: piezo sound checked skipped! Message: {}".format(check))
        else:
            self.piezo_check_passed = 1
            logging.info("piezo sound check passed!")
        self.instr.turn_off()

    def set_wave(self, waveform=None, frequency=None, amplitude=None, channel=1, frequency_unit='Hz',
                 amplitude_unit='V', offset=None, offset_unit='V', duty=None):
        if waveform is not None:
            if waveform == "square_pol":
                self.set_square_polarity(duty=duty, channel=channel)
            else:
                self.instr.set_waveform(waveform=waveform, channel=channel)
                text = "AFG Channel {}: waveform set to: {}".format(channel, waveform)
                logging.info(text)
                # sleep(0.1)
        if frequency is not None:
            self.instr.set_frequency(frequency=frequency, unit=frequency_unit, channel=channel)
            text = "AFG Channel {}: frequency set to: {} {}".format(channel, frequency, frequency_unit)
            logging.info(text)
            # sleep(0.1)
        if amplitude is not None:
            self.instr.set_amplitude(amplitude=amplitude, unit=amplitude_unit, channel=channel)
            text = "AFG Channel {}: amplitude set to: {} {}".format(channel, amplitude, amplitude_unit)
            logging.info(text)
            # sleep(0.1)
        if offset is not None:
            self.instr.set_offset(offset=offset, unit=offset_unit, channel=channel)
            text = "AFG Channel {}: DC offset set to: {} {}".format(channel, offset, offset_unit)
            logging.info(text)
            # sleep(0.1)

    def set_square_polarity(self, duty=0.5, channel=1, resolution=1000):
        self.instr.square_polarity(duty=duty, channel=channel)
        text = "AFG Channel {}: waveform set to square polarity with a duty of {}".format(channel, duty)
        logging.info(text)

    def set_arbitrary_waveform(self, wave_form, channel=1):
        self.instr.arbitrary_wave(wave_form, channel)
        text = "AFG Channel {}: arbitrary waveform set".format(channel)
        logging.info(text)

    def turn_on(self, channel=1):
        self.instr.turn_on(channel=channel)
        text = "AFG Channel {}: output turned on".format(channel)
        logging.info(text)

    def turn_off(self, channel=1):
        self.instr.turn_off(channel=channel)
        text = "AFG Channel {}: output turned off".format(channel)
        logging.info(text)

if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO)

    afg = FunctionGenerator()
    # afg.check_piezo()
    afg.set_wave(waveform='sine', frequency=40, amplitude=1, channel=1, offset=10, offset_unit='mV')

