# Arduino serial communication
import serial
from time import time, sleep
import logging
import matplotlib.pyplot as plt
from threading import Thread
import numpy as np

class ArduinoSerial:
    def __init__(self, COM='COM4', baudrate=9600, timeout=1):
        self.port = COM
        self.baudrate = baudrate
        self.timeout = timeout

        self.run_condition = False  # thread running condition

        self.read_value = np.zeros((1, 2))
        self.read_values = np.empty((0, 2))

        self.serial = serial.Serial(self.port, self.baudrate, timeout=self.timeout)
        self.initial_time = time()

    def close(self):
        self.serial.close()

    def read_line(self, print_to_console=False):
        # Read arduino serial and save as list in self.read_values ([time, read]),
        self.read_value = np.zeros((1, 2))
        self.read_value[0, 0] = time() - self.initial_time

        line = self.serial.readline()  # read as bytes
        if line:
            string = line.decode()  # convert the byte string to a unicode string
            stripped_string = string.strip("\r\n")  # remove linebreak and convert string to float

            # check if only one "." in string and all other chars are digits (strip the "-" to handle negative numbers")
            if stripped_string.replace('.', '', 1).strip("-").isdigit():
                self.read_value[0, 1] = float(stripped_string)

            # print if print_to_console is True
            if print_to_console is True:
                print(string)

    def measurement_thread(self):
        self.run_condition = True
        logging.info("Arduino measurements started (thread)")
        self.initial_time = time()
        while self.run_condition:
            try:
                self.read_line()
                self.read_values = np.vstack((self.read_values, self.read_value))
            except:
                logging.INFO("ValueError: could not convert string to float")

        logging.info("Measurement thread: stopped")
        # print("Measurement thread: stopped")

    def start_measurement_thread(self):
        y = Thread(target=self.measurement_thread)
        y.start()

    def stop_measurement_thread(self):
        self.run_condition = False

    def save_data_to_file(self, filename='arduino_data.csv', header='', delimiter=','):
        # save pressure and flow data to file
        np.savetxt(filename, self.read_values, header=header, delimiter=delimiter)

    def plot_data(self, save_to_fig=False, save_file_name="arduino_plot.png", legend="", title="", show=True):
        # Plot results
        fig, axes = plt.subplots(1, 1)
        time = self.read_values[:, 0]   # time as x-axis

        if legend == "":
                legend = ["Voltage measured by arduino with ADS1115 ADC"]

        # plot serial reads vs time
        axes.plot(time, self.read_values[:, 1])

        if title == "":
            axes.set_title('arduino voltage measurement')

        axes.set(xlabel='time [s]', ylabel='voltage [mV]')
        axes.legend(legend)

        # save image of plot is True
        if save_to_fig is True:
            plt.savefig(save_file_name)

        if show is True:
            plt.show()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    arduino = ArduinoSerial()
    arduino.start_measurement_thread()
    sleep(4)
    arduino.stop_measurement_thread()

    # print(arduino.read_values)

    arduino.save_data_to_file()
    arduino.plot_data()

    arduino.close()
