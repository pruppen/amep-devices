#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name='devices',
    version='0.5.0',
    description='Python library to control equipment needed for automated microfluidic electroporation.',
    url='https://git.bsse.ethz.ch/pruppen/amep-devices',
    author='Peter Ruppen',
    author_email='peter.ruppen@bsse.ethz.ch',
    license='GNU 3.0',
    packages=find_packages(),
    install_requires=['pyvisa', 'pyserial', 'numpy'],

)
